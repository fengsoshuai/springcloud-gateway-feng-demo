package org.feng.fengserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FengServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(FengServerApplication.class, args);
    }

}
