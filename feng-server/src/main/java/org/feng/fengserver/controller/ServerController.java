package org.feng.fengserver.controller;

import lombok.extern.slf4j.Slf4j;
import org.feng.fengserver.emums.ResultEnum;
import org.feng.fengserver.request.ListUserReq;
import org.feng.fengserver.vo.ResultVo;
import org.feng.fengserver.vo.UserVo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

/**
 * 控制器
 */
@Slf4j
@RestController
@RequestMapping("/server")
public class ServerController {

    @Value("${server.port}")
    private Integer port;

    @Value("${spring.application.name}")
    private String applicationName;

    @PostMapping("/list/server1/user")
    public ResultVo getServer1Users(@RequestBody ListUserReq req) {
        return new ResultVo(getUserVoList(req), ResultEnum.SUCCESS);
    }

    @PostMapping("/list/server2/user")
    public ResultVo getServer2Users(@RequestBody ListUserReq req) {
        return new ResultVo(getUserVoList(req), ResultEnum.SUCCESS);
    }

    private List<UserVo> getUserVoList(ListUserReq req) {
        log.info("{}:{}接收到请求参数：{}", applicationName, port, req);
        UserVo userVo1 = new UserVo();
        userVo1.setName("userVo1用户");
        userVo1.setPassword("123456");
        userVo1.setPhone("13254654554");
        userVo1.setBirthday(LocalDate.of(2020, 12, 12));
        UserVo userVo2 = new UserVo();
        userVo2.setName("userVo2用户");
        userVo2.setPassword("qwerty123");
        userVo2.setPhone("13254654552");
        userVo2.setBirthday(LocalDate.of(2020, 12, 12));
        UserVo userVo3 = new UserVo();
        userVo3.setName("userVo3用户");
        userVo3.setPassword("poiuyt21#");
        userVo3.setPhone("13254654551");
        userVo3.setBirthday(LocalDate.of(2020, 12, 12));

        return List.of(userVo1, userVo2, userVo3);
    }
}
