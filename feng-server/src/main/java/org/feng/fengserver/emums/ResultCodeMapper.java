package org.feng.fengserver.emums;

/**
 * 响应码映射接口
 *
 * @author feng
 */
public interface ResultCodeMapper {

    int getCode();

    String getMsg();
}
