package org.feng.fengserver.emums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 结果枚举
 *
 * @author feng
 */
@AllArgsConstructor
@Getter
public enum ResultEnum implements ResultCodeMapper {

    SUCCESS(0, "成功"),
    ERROR(500, "服务内部错误");

    private final int code;

    private final String msg;
}
