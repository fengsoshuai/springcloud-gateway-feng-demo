package org.feng.fengserver.request;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * 查用户列表的请求体
 *
 * @author feng
 */
@Data
public class ListUserReq implements Serializable {

    @Serial
    private static final long serialVersionUID = -7229649591526082939L;

    private String username;

    private LocalDate registerBegin;

    private LocalDate registerEnd;
}
