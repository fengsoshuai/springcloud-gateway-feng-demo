package org.feng.fengserver.vo;

import lombok.Data;
import lombok.NonNull;
import org.feng.fengserver.emums.ResultCodeMapper;

import java.io.Serial;
import java.io.Serializable;

/**
 * 响应结果vo
 *
 * @author feng
 */
@Data
public class ResultVo implements Serializable {
    @Serial
    private static final long serialVersionUID = -7953951061518179791L;

    private int code;

    private String msg;

    private Object data;

    public ResultVo(Object data, @NonNull ResultCodeMapper resultCode) {
        this.data = data;
        this.code = resultCode.getCode();
        this.msg = resultCode.getMsg();
    }

    public ResultVo(@NonNull ResultCodeMapper resultCode) {
        this(null, resultCode);
    }
}
