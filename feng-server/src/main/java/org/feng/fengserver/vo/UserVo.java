package org.feng.fengserver.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * 用户信息vo
 *
 * @author feng
 */
@Data
@Accessors(chain = true)
public class UserVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 2675066390401225614L;

    private String name;

    private String password;

    private LocalDate birthday;

    private String phone;
}
