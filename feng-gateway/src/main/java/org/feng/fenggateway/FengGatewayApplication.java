package org.feng.fenggateway;

import jakarta.annotation.Resource;
import org.feng.fenggateway.config.SecureProperties;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@ConfigurationPropertiesScan
@SpringBootApplication
public class FengGatewayApplication implements CommandLineRunner {

    @Resource
    private SecureProperties secureProperties;

    public static void main(String[] args) {
        SpringApplication.run(FengGatewayApplication.class, args);
    }

    @Override
    public void run(String... args) {
        secureProperties.checkSupportedAlgorithm();
    }
}
