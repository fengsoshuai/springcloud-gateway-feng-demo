package org.feng.fenggateway.secure;

import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

/**
 * 加解密组件工厂
 *
 * @author feng
 */
@Slf4j
public class SecureComponentFactory {

    public static final Map<SymmetricAlgorithm, SecureComponent> SECURE_COMPONENT_MAP;

    static {
        SECURE_COMPONENT_MAP = new HashMap<>(16);
    }

    public static void registerBean(SecureComponent secureComponent) {
        log.info("注册加解密算法：{}", secureComponent);
        SECURE_COMPONENT_MAP.put(secureComponent.getAlgorithmType(), secureComponent);
    }

    public static SecureComponent get(SymmetricAlgorithm algorithm) {
        return SECURE_COMPONENT_MAP.getOrDefault(algorithm, new SecureAESComponent());
    }

    public static boolean isSupportedAlgorithm(SymmetricAlgorithm algorithm) {
        return SECURE_COMPONENT_MAP.containsKey(algorithm);
    }
}
