package org.feng.fenggateway.secure;

import cn.hutool.crypto.Mode;
import cn.hutool.crypto.Padding;
import cn.hutool.crypto.symmetric.AES;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;

/**
 * AES加解密组件
 *
 * @author feng
 */
@Component
public class SecureAESComponent implements SecureComponent {

    /**
     * 生成密钥，16、24、32位都行
     */
    private final static byte[] SECURE_KEY = "r4oz0f3kfk5tgyui".getBytes(StandardCharsets.UTF_8);

    /**
     * 偏移量，必须16位
     */
    private final static String IV = "r21g95kdsd423gy6";

    private final static AES AES_INSTANCE = new AES(Mode.CTS, Padding.PKCS5Padding, SECURE_KEY, IV.getBytes(StandardCharsets.UTF_8));

    @Override
    public String encrypt(String originalText) {
        return AES_INSTANCE.encryptHex(originalText);
    }

    @Override
    public String decrypt(String encryptedText) {
        return AES_INSTANCE.decryptStr(encryptedText);
    }

    @Override
    public SymmetricAlgorithm getAlgorithmType() {
        return SymmetricAlgorithm.AES;
    }
}
