package org.feng.fenggateway.secure;

import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import jakarta.annotation.PostConstruct;

/**
 * 加解密组件
 *
 * @author feng
 */
public interface SecureComponent {

    /**
     * 加密
     *
     * @param originalText 原文
     * @return 密文
     */
    String encrypt(String originalText);

    /**
     * 解密
     *
     * @param encryptedText 密文
     * @return 解密后的明文
     */
    String decrypt(String encryptedText);

    /**
     * 获取加解密算法类型
     *
     * @return 加解密算法类型
     */
    SymmetricAlgorithm getAlgorithmType();


    @PostConstruct
    default void registerToFactory() {
        SecureComponentFactory.registerBean(this);
    }
}
