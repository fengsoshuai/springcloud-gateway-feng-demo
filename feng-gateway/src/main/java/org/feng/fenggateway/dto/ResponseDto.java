package org.feng.fenggateway.dto;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 响应传输对象
 *
 * @author feng
 */
@Data
public class ResponseDto implements Serializable {
    @Serial
    private static final long serialVersionUID = -6354714160436354659L;

    private Integer code;

    private String msg;

    private Object data;
}
