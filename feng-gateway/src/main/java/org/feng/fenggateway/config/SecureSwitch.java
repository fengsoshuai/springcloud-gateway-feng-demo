package org.feng.fenggateway.config;

import lombok.Data;

/**
 * 加解密开关
 *
 * @author feng
 */
@Data
public class SecureSwitch {

    /**
     * 是否启用：默认为否
     */
    private Boolean enable = Boolean.FALSE;
}
