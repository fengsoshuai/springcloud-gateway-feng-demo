package org.feng.fenggateway.config;

import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.feng.fenggateway.secure.SecureComponentFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Objects;

/**
 * 加解密属性配置
 *
 * @author feng
 */
@Slf4j
@Data
@ConfigurationProperties(prefix = SecureProperties.SECURE_PROPERTIES_PREFIX)
public class SecureProperties {

    public static final String SECURE_PROPERTIES_PREFIX = "feng.gateway.secure";

    /**
     * 算法
     */
    private SymmetricAlgorithm algorithm;

    /**
     * 请求开关
     */
    private SecureSwitch requestSwitch;

    /**
     * 响应开关
     */
    private SecureSwitch responseSwitch;

    public void checkSupportedAlgorithm() {
        log.info("校验是否支持算法：{}", algorithm);
        if (Objects.isNull(algorithm)) {
            return;
        }
        boolean supportedAlgorithm = SecureComponentFactory.isSupportedAlgorithm(algorithm);
        if (!supportedAlgorithm) {
            throw new UnsupportedOperationException("不支持的算法");
        }
        log.info("校验是否支持算法：校验通过");
    }

    /**
     * 是否启用解密请求参数
     *
     * @return 默认为否，其他情况看配置
     */
    public boolean enableDecryptRequestParam() {
        if (Objects.isNull(requestSwitch)) {
            return false;
        }
        return requestSwitch.getEnable();
    }

    /**
     * 是否启用加密响应参数
     *
     * @return 默认为否，其他情况看配置
     */
    public boolean enableEncryptResponseParam() {
        if (Objects.isNull(responseSwitch)) {
            return false;
        }
        return responseSwitch.getEnable();
    }
}
